const express = require("express");
const fs = require("fs");
const app = express();
const path = require('path');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin,locale,Authorization, userId, token, password, X-Requested-With, Content-Type, Accept");
  next();
});

function readFile(filename, res) {
  setTimeout(function() {
    fs.readFile(path.resolve('json/' + filename + ".json"), 'utf8', (err, data) => {
      if (err) {
        data = err;
        if (filename) {
          n = filename.lastIndexOf("-");
          filename = filename.substring(0, n)
          readFile(filename, res);
        } else
          res.status(404).send(data);
      } else
        res.send(data);
    });
  }, 1000);
}
app.get('/:file1/:file2*?/:file3*?', function(req, res) {
  var filename = req.params["file1"];
  if (req.params["file2"])
    filename += '-' + req.params["file2"];
  else if (req.params["file3"])
    filename += '-' + req.params["file3"];
  readFile(filename, res);
});
app.post('/:file1/:file2*?/:file3*?', function(req, res) {
  var filename = req.params["file1"];
  if (req.params["file2"])
    filename += '-' + req.params["file2"];
  else if (req.params["file3"])
    filename += '-' + req.params["file3"];
  readFile(filename, res);
})
app.listen(3000, function() {
  console.log('Example app listening on port 3000!')
})
